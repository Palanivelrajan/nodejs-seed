import { Test, TestingModule } from '@nestjs/testing';
import * as request from 'supertest';
import { AppModule } from './../src/app/app.module';

describe('AppController (e2e)', () => {
  let app;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });
  //since there is no implementation for this end point, expect is set to 404 and need to change if we implement the endpoint.
  it('/ (GET)', () => {
    return request(app.getHttpServer())
      .get('/')
      .expect(404);
    //.expect('Hello World!');
  });

  afterAll(async () => {
    await app.close();
  });
});
