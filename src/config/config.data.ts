import { readFileSync } from 'fs';
import { Config } from './config.Interface';
//import { config } from 'winston';
const appPackage = readFileSync(`${__dirname}/../../package.json`, {
  encoding: 'utf8',
});

const appData = JSON.parse(appPackage);

export const config: Config = {
  appRootPath: `${__dirname}/../app`,
  version: appData.version,
  name: appData.name,
  description: appData.description,
  uuid: process.env.APP_UUID,
  isProduction: process.env.NODE_ENV === 'production',
  assetsPath: `${__dirname}/../assets`,
  port: parseInt(process.env.APP_PORT, 10),
  host: process.env.APP_HOST,
  apiRootUrl: "http://localhost:3001",
  requestTimeout: 60000,
  logger: {
    level: process.env.APP_LOGGER_LEVEL,
    IsFileLog: true,
  },
};
