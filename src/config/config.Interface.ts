export interface Config {
    appRootPath: string;
    version: string;
    name: string;
    description: string;
    uuid: string;
    isProduction: boolean;
    assetsPath: string;
    port: number;
    host: string;
    apiRootUrl: string;
    requestTimeout: number;
    logger: {
        level: string;
        IsFileLog: boolean;
    };
}
