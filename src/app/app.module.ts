import { Module } from '@nestjs/common';
import { HealthcheckModule } from './healthcheck/healthcheck.module';
import { ApiModule } from './core/api/api.module';
//import { AppLoggerService } from './helpers/logger/app.logger.service';
import { ItemModule } from './item/item.module';
import { AppLoggerService, SynapseBffCoreModule } from 'synapse.bff.core';

@Module({
  imports: [HealthcheckModule, ItemModule, LoggersModule, ApiModule,FiltersModule,InterceptorsModule, LoggersModule ],
  controllers: [],
  providers: [],
  exports:[LoggersModule]
})
export class AppModule {
  constructor(private readonly logger: AppLoggerService) {
    let message = {
      Title: 'Initialize constructor',
      Type: 'Info',
      Detail: 'Initialize constructor',
      Status: 'Status',
    };
    this.logger.log(message);
  }
}
