import { INestApplication, INestApplicationContext } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import {
  FastifyAdapter,
  NestFastifyApplication,
} from '@nestjs/platform-fastify';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import helmet from 'helmet';
import { config } from '../config';
import { AppModule } from './app.module';

import { AppLoggerService, TimeoutInterceptor, TransformInterceptor,LoggingInterceptor,AllExceptionsFilter, HttpExceptionFilter,RequestTimeoutExceptionFilter, BadRequestExceptionFilter} from 'synapse.bff.core';

export class AppDispatcher {
  private app: INestApplication;
  private logger = new AppLoggerService(AppDispatcher.name);
  async dispatch(): Promise<void> {
    await this.createServer();
    return this.startServer();
  }

  async shutdown(): Promise<void> {
    await this.app.close();
  }

  public getContext(): Promise<INestApplicationContext> {
    return NestFactory.createApplicationContext(AppModule);
  }

  private async createServer(): Promise<void> {
    this.app = await NestFactory.create<NestFastifyApplication>(
      AppModule,
      new FastifyAdapter({ logger: new AppLoggerService('Nest') }),
    );

    this.app.enableCors();
    this.app.useGlobalInterceptors(
      new TimeoutInterceptor(),
      new TransformInterceptor(),
      new LoggingInterceptor(this.logger),
    );
    this.app.useGlobalFilters(
      new AllExceptionsFilter(this.logger),
      new HttpExceptionFilter(this.logger),
      new BadRequestExceptionFilter(this.logger),
      new RequestTimeoutExceptionFilter(),
    );

    if (config.isProduction) {
      this.app.use(helmet());
    }
    const options = new DocumentBuilder()
      .setTitle(config.name)
      .setDescription(config.description)
      .setVersion(config.version)
      .addBearerAuth()
      .build();

    const document = SwaggerModule.createDocument(this.app, options);
    SwaggerModule.setup('/swagger', this.app, document);
  }

  private async startServer(): Promise<void> {
    await this.app.listen(config.port, config.host);
    let message = {
      Title: `Swagger is exposed at http://${config.host}:${
        config.port
      }/swagger`,
      Type: 'Info',
      Detail: 'Initialize constructor',
      Status: 'Status',
    };

    this.logger.log(message);
    message.Title = `Server is listening http://${config.host}:${config.port}`;
    this.logger.log(message);
  }
}
