import { config } from '../config';
import { transports, createLogger, format, Logger } from 'winston';
import { join } from 'path';
import { DebugTransport } from './logger/transport/debugg.transport';
import { ClientApiMetricsTransport } from './logger/transport/client.api.metrics.transport';

export function loggeroption() {
    if (config.logger.IsFileLog) {
        return {
            level: config.logger.level,
            format: format.json(),
            transports: [
                new transports.File({ dirname: join(__dirname, './../log/debug/'), filename: 'debug.log', level: 'debug', maxsize: 100 * 1024, maxFiles: 10 }),
                new transports.File({ dirname: join(__dirname, './../log/error/'), filename: 'error.log', level: 'error', maxsize: 100 * 1024, maxFiles: 10 }),
                new transports.File({ dirname: join(__dirname, './../log/info/'), filename: 'info.log', level: 'info', maxsize: 100 * 1024, maxFiles: 10 }),
            ],
        };
    }
    else {
        return {
            level: config.logger.level,
            format: format.json(),
            transports: [
                new DebugTransport({ level: 'info' }),
                new ClientApiMetricsTransport({ level: 'info' }),
            ],
        };
    }
}