import { LoggerService, Injectable, Optional } from '@nestjs/common';
import { transports, createLogger, format, Logger } from 'winston';

import { join } from 'path';
import { DebugTransport } from './transport/debugg.transport';
import { ClientApiMetricsTransport } from './transport/client.api.metrics.transport';
import { logmessage } from './logmessage'
import { arrayExpression } from '@babel/types';
import { config } from './../../config';
@Injectable()
export class AppLoggerService implements LoggerService {
    private logger: Logger;
    constructor(@Optional() label?: string) {
        this.initializeLogger(label);
    }
    initializeLogger(label?: string) {


        if (config.logger.IsFileLog) {
            this.logger = createLogger({
                level: config.logger.level,
                format: format.json(),
                transports: [
                    new transports.File({ dirname: join(__dirname, './../log/debug/'), filename: 'debug.log', level: 'debug', maxsize: 100 * 1024, maxFiles: 10 }),
                    new transports.File({ dirname: join(__dirname, './../log/error/'), filename: 'error.log', level: 'error', maxsize: 100 * 1024, maxFiles: 10 }),
                    new transports.File({ dirname: join(__dirname, './../log/info/'), filename: 'info.log', level: 'info', maxsize: 100 * 1024, maxFiles: 10 }),
                ],
            });
        }
        else {
            this.logger = createLogger({
                level: config.logger.level,
                format: format.json(),
                transports: [
                    new DebugTransport({ level: 'info' }),
                    new ClientApiMetricsTransport({ level: 'info' }),
                ],
            });
        }
        // if (process.env.NODE_ENV !== 'production') {
        //     this.logger.add(
        //         new transports.Console({
        //             format: format.simple(),
        //         }),
        //     );
        // }
    }
    error(message: logmessage, trace: string) {
        this.logger.log("error", "error", message);
    }

    warn(message: logmessage) {
        this.logger.log("warn", "warn : ", message);
    }

    log(message: logmessage) {
        this.logger.log("info", "info :", message);
    }

    verbose(message: logmessage) {
        this.logger.log("verbose", "verbose :", message);
    }

    debug(message: logmessage) {
        this.logger.log("debug", "debug :", message);
    }
}

