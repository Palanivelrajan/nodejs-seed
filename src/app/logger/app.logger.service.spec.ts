import { Test, TestingModule } from '@nestjs/testing';
import { AppLoggerService } from './app.logger.service';
import { config } from './../../config';
import {logmessage} from './logmessage';

describe('LoggerService', () => {
  let service: AppLoggerService;


  let message:logmessage = {
    "Title": "This is unit testing",
    "Type": "Unit Test",
    "Detail": "Unit Test - This is detail of the log",
    "Status": "Open",
  }
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AppLoggerService],
    }).compile();

    service = module.get<AppLoggerService>(AppLoggerService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('initializeLogger should be called', () => {
    expect(service.initializeLogger()).toBeCalled;
  });

  it('log should be called', () => {
    message.Title = "Log"
    expect(service.log(message)).toBeCalled;
  });
  it('error should be called', () => {
    message.Title = "error"
    expect(service.error(message,"")).toBeCalled;
  });
  it('debug should be called', () => {
    message.Title = "debug"
    expect(service.debug(message)).toBeCalled;
  });
  it('verbose should be called', () => {
    message.Title = "verbose"
    expect(service.verbose(message)).toBeCalled;
  });
  it('warn should be called', () => {
    message.Title = "warn"
    expect(service.warn(message)).toBeCalled;
  });
});
