import * as winstonTransport from 'winston-transport';
export class DebugTransport extends winstonTransport {
    constructor(options) {
        super(options);
    }
    log(info, callback) {
        setImmediate(() => {
            this.emit('logged', info);
        });
        callback();
    }
}