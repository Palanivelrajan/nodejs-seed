import { Module } from '@nestjs/common';
import { ItemController } from './item.controller';
import { ItemService } from './item.service';
import { ApiModule } from '../core/api/api.module';
import { LoggersModule } from 'synapse.bff.core';

@Module({
  imports:[ApiModule, LoggersModule],
  controllers: [ItemController],
  providers: [ItemService],
})
export class ItemModule {}
