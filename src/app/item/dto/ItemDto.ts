export class ItemDto {
    Code: string;
    Name: string;
    Price: number;
    Category: string;
}