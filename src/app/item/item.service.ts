import { Injectable } from '@nestjs/common';
import { ApiService } from './../core/api/api.service'
import * as itemsDTO from './dto';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AxiosResponse } from 'axios';
import { AppLoggerService } from 'synapse.bff.core';
@Injectable()
export class ItemService {

  constructor(private _apiService: ApiService, private readonly logger: AppLoggerService) { }

  getItems(code: string): Observable<itemsDTO.ItemDto[]> {
    const path = '/item/getitem' ;
    return this._apiService.get(path).pipe(
      map(resp => {
        return resp.data;
      }));
  }

  addItem(itemDto: itemsDTO.ItemDto): Observable<itemsDTO.ItemDto[]> {
    const path = '/item/additem';

    let message = {
      Title: 'This is Item test',
      Type: 'Error',
      Detail: '/item/additem',
      Status: 'Status',
      Extension: '',
    };

    this.logger.log(message);

    return this._apiService.post(path, itemDto).pipe(
      map(resp => {
        return resp.data;
      }));


  }

  updateItem(itemDto: itemsDTO.ItemDto): Observable<itemsDTO.ItemDto[]> {
    const path = '/item/updateitem';
    return this._apiService.put(path, itemDto).pipe(
      map(resp => {
        return resp.data;
      }));
  }

  deleteItem(code: string): Observable<itemsDTO.ItemDto[]> {
    const path = '/item/deleteitem';
    return this._apiService.delete(path, code).pipe(
      map(resp => {
        return resp.data;
      }));
  }

}
