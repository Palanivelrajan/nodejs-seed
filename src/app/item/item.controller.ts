import { Controller, Get, Query, Post, Body, Put, Param, Delete } from '@nestjs/common';
import { ItemDto } from './dto';
import { config } from './../../config';
import { AppLoggerService } from 'synapse.bff.core';
import { ItemService } from './item.service';

@Controller('item')
export class ItemController {
    private start: number;

    constructor(private readonly logger: AppLoggerService, private readonly itemService: ItemService) {
        this.start = Date.now();
    }
    @Post("create")
    create(@Body() itemDto: ItemDto) {
        return this.itemService.addItem(itemDto);
    }

    // @Get()
    // findAll(@Query() query: ListAllEntities) {
    //     return `This action returns all cats (limit: ${query.limit} items)`;
    // }

    @Get(':code')
    findOne(@Param('code') code: string) {
        return this.itemService.getItems(code);
    }

    @Put("update")
    update(@Body() itemDto: ItemDto) {
        return this.itemService.updateItem(itemDto);
    }

    @Delete(':code')
    remove(@Param('code') code: string) {
        return this.itemService.deleteItem(code);
    }
}