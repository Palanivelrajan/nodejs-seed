import { Test, TestingModule } from '@nestjs/testing';
import { AppLoggerService } from './app.logger.service';
import { config } from './../../../config';
import { logmessage } from './logmessage';

describe('LoggerService', () => {
  let appLoggerService: AppLoggerService;


  let message: logmessage = {
    "Title": "This is unit testing",
    "Type": "Unit Test",
    "Detail": "Unit Test - This is detail of the log",
    "Status": "Open",
  }
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AppLoggerService],
    }).compile();

    appLoggerService = module.get<AppLoggerService>(AppLoggerService);
  });

  it('should be defined', () => {
    expect(appLoggerService).toBeDefined();
  });

  it('initializeLogger should be called', () => {
    config.logger.IsFileLog = true;
    jest.mock('./../../../config', () => jest.fn().mockReturnValue(config));
    appLoggerService.initializeLogger();
    expect(appLoggerService.getlogger()).toBe(3);
  });

  it('log should be called', () => {
    message.Title = "Log"
    jest.spyOn(appLoggerService, 'log').mockImplementation(jest.fn());
    expect(appLoggerService.log(message)).toBeCalled;
  });
  it('error should be called', () => {
    message.Title = "error"
    jest.spyOn(appLoggerService, 'error').mockImplementation(jest.fn());
    expect(appLoggerService.error(message, "")).toBeCalled;
  });
  it('debug should be called', () => {
    message.Title = "debug"
    jest.spyOn(appLoggerService, 'debug').mockImplementation(jest.fn());
    expect(appLoggerService.debug(message)).toBeCalled;
  });
  it('verbose should be called', () => {
    message.Title = "verbose"
    jest.spyOn(appLoggerService, 'verbose').mockImplementation(jest.fn());
    expect(appLoggerService.verbose(message)).toBeCalled;
  });
  it('warn should be called', () => {
    message.Title = "warn"
    jest.spyOn(appLoggerService, 'warn').mockImplementation(jest.fn());
    expect(appLoggerService.warn(message)).toBeCalled;
  });
});
