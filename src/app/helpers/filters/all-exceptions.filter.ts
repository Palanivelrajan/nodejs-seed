import {
  ArgumentsHost, Catch, ExceptionFilter, HttpException, HttpStatus, Inject} from '@nestjs/common';
import { Logger } from 'winston';
//import { AppLoggerService } from '../logger/app.logger.service';

@Catch()
export class AllExceptionsFilter implements ExceptionFilter {
  //private logger = new AppLoggerService(AllExceptionsFilter.name);
  constructor(@Inject('winston') private readonly logger: Logger) {
  }
  catch(exception: any, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    const request = ctx.getRequest();

    let message = {
      Title: exception.name,
      Type: 'Error',
      Detail: exception.message,
      Status: 'Status',
      Extension: '',
    };
    this.logger.error(JSON.stringify(message), '');

    const status =
      exception instanceof HttpException
        ? exception.getStatus()
        : HttpStatus.INTERNAL_SERVER_ERROR;

    response.status(status).send({
      statusCode: status,
      timestamp: new Date().toISOString(),
      path: request.url,
    });
  }
}
