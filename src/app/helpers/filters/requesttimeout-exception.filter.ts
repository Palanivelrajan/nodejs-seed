import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpStatus,
  RequestTimeoutException,
} from '@nestjs/common';
import { AppLoggerService } from '../logger/app.logger.service';

@Catch(RequestTimeoutException)
export class RequestTimeoutExceptionFilter<T> implements ExceptionFilter {
  //private logger = new AppLoggerService(RequestTimeoutException.name);
  // catch(exception: T, host: ArgumentsHost) {}
  catch(exception: RequestTimeoutException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    const request = ctx.getRequest();

    const status =
    exception instanceof RequestTimeoutException
        ? exception.getStatus()
        : HttpStatus.INTERNAL_SERVER_ERROR;

    let message = {
      Title: exception.message.error,
      Type: 'Exception - RequestTimeoutException',
      Detail: exception.message,
      Status: '',
    };

   // this.logger.error(message, '');

    response.code(status).send({
      statusCode: status,
      timestamp: new Date().toISOString(),
    });
  }
}
