import {
  ArgumentsHost,
  BadRequestException,
  Catch,
  ExceptionFilter,
  HttpStatus,
} from '@nestjs/common';
import { AppLoggerService } from '../logger/app.logger.service';

@Catch(BadRequestException)
export class BadRequestExceptionFilter implements ExceptionFilter {
 //private logger = new AppLoggerService(BadRequestExceptionFilter.name);

  catch(exception: BadRequestException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    const request = ctx.getRequest();

    const status =
      exception instanceof BadRequestException
        ? exception.getStatus()
        : HttpStatus.INTERNAL_SERVER_ERROR;
    //let status = HttpStatus.BAD_REQUEST;

    let message = {
      Title: exception.message.error,
      Type: 'Exception - BadRequestExceptionFilter',
      Detail: exception.message,
      Status: '',
    };

    //this.logger.error(message, '');

    response.code(status).send({
      statusCode: status,
      ...(exception.getResponse() as object),
      timestamp: new Date().toISOString(),
    });
  }
}
