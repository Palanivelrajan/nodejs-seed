import { RequestTimeoutExceptionFilter } from './requesttimeout-exception.filter';

describe('RequesttimeoutExceptionFilter', () => {
  it('should be defined', () => {
    expect(new RequestTimeoutExceptionFilter()).toBeDefined();
  });
});
