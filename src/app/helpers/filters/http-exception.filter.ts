import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException,
} from '@nestjs/common';
import { AppLoggerService } from '../logger/app.logger.service';

@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {
 // private logger = new AppLoggerService(HttpExceptionFilter.name);

  catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    let status;

    if (typeof exception.message === 'string') {
      exception = new HttpException(
        { error: 'Undefined', message: exception.message },
        status,
      );
    }

    if (exception.getStatus) {
      status = exception.getStatus();
    }

    let message = {
      Title: exception.message.error,
      Type: 'Exception - HttpExceptionFilter',
      Detail: exception.message,
      Status: '',
    };

    //this.logger.error(message, exception.stack);

    response.code(status).send({
      statusCode: status,
      ...(exception.getResponse() as object),
      timestamp: new Date().toISOString(),
    });
  }
}
