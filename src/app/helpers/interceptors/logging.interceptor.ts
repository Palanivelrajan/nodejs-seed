import { Injectable, NestInterceptor, ExecutionContext, CallHandler } from '@nestjs/common';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AppLoggerService } from '../logger/app.logger.service';
@Injectable()
export class LoggingInterceptor implements NestInterceptor {
  constructor(private logger = new AppLoggerService()) { }
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    console.log('Before...');
    const ctx = context.switchToHttp();
    const now = Date.now();
    const request = ctx.getRequest().body || {};
    const method = ctx.getRequest().raw.method;
    const url = ctx.getRequest().raw.url;
    return next
      .handle()
      .pipe(
        tap(
          (response) => {

            let logdet = {
              requestdatetime: now,
              duration: Date.now() - now,
              response,
              request,
              method,
              url
            };

            let message = {
              Title: 'Logger.interceptor',
              Type: 'Log',
              Detail: JSON.stringify(logdet),
              Status: 'Status',
              Extension: '',
            };

            this.logger.verbose(message);
          }
        ),
      );
  }
}