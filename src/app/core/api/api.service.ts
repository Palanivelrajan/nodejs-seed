import { throwError as observableThrowError, Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HttpService, Injectable } from '@nestjs/common';
import { config } from '../../../config'
import { AxiosResponse } from 'axios';

@Injectable()
export class ApiService {
    private readonly httpService: HttpService = new HttpService();
    constructor() { }

    private formatErrors(error: any) {
        console.log('formatErrors :', error);
        return observableThrowError(error);
    }
    post(path: string, body: Object = {}): Observable<AxiosResponse> {
        return this.httpService.post(config.apiRootUrl + path, body).pipe(
            catchError(this.formatErrors),
            map((res: AxiosResponse) => res));
    }
    get(path: string, body: Object = {}): Observable<AxiosResponse<any>> {
        let url = config.apiRootUrl + path;
        return this.httpService.get(url).pipe(
            catchError(this.formatErrors),
            map((res) => res));
    }
    put(path: string, body: any = {}): Observable<AxiosResponse> {
        return this.httpService.put(config.apiRootUrl + path, body).pipe(
            catchError(this.formatErrors),
            map((res: AxiosResponse) => res));
    }
    delete(path: string, body: any = {}): Observable<AxiosResponse> {
        return this.httpService.delete(config.apiRootUrl + path).pipe(
            catchError(this.formatErrors),
            map((res: AxiosResponse) => res));
    }

}
