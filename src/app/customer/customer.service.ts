import { Injectable } from '@nestjs/common';
import { AppLoggerService } from '../helpers/logger/app.logger.service';
import { Customer } from './customer.entity';
import { Observable } from 'rxjs';
import { InjectRepository, InjectConnection, InjectEntityManager } from '@nestjs/typeorm';
import { Repository, Connection, EntityManager } from 'typeorm';

@Injectable()
export class CustomerService {
    constructor(
        @InjectRepository(Customer, 'mysqlConnection')
        private readonly CustomerRepository: Repository<Customer>,
    ) { }

    async findAll(): Promise<Customer[]> {
        return await this.CustomerRepository.find();
    }

    getItems(ID: string) {

    }

    addItem(itemDto: Customer) {

    }

    updateItem(itemDto: Customer) {

    }

    deleteItem(ID: string) {

    }



}
