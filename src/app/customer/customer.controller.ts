import { CustomerService } from './customer.service';
import { Controller, Get, Query, Post, Body, Put, Param, Delete } from '@nestjs/common';
import { AppLoggerService } from '../helpers/logger/app.logger.service';
import { Customer } from './customer.entity';

@Controller('customer')
export class CustomerController {
    private start: number;

    constructor(private readonly logger: AppLoggerService, private readonly customerService: CustomerService) {
        this.start = Date.now();
    }
    @Post("create")
    create(@Body() customer: Customer) {
       return this.customerService.addItem(customer);
    }

    @Get(':ID')
    findOne(@Param('ID') ID: string) {
        return this.customerService.findAll();
    }

    @Put("update")
    update(@Body() customer: Customer) {
        return this.customerService.updateItem(customer);
    }

    @Delete(':ID')
    remove(@Param('ID') ID: string) {
        return this.customerService.deleteItem(ID);
    }


}
