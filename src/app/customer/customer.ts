import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Customer {
    @PrimaryGeneratedColumn()
    ID: number;
  
    @Column({ length: 50 })
    Name: string;
  
    @Column()
    CreatedDate: Date;
  
    @Column({ length: 50 })
    CreatedBy: string ;

}
