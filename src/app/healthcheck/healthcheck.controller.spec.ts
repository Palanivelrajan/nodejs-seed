import { Test, TestingModule } from '@nestjs/testing';
import { AppLoggerService } from 'synapse.bff.core';
import { HealthcheckController } from './healthcheck.controller';

describe('Healthcheck Controller', () => {
  let controller: HealthcheckController;
  let appLoggerService: AppLoggerService;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [HealthcheckController],
      providers: [AppLoggerService],
    }).compile();

    controller = module.get<HealthcheckController>(HealthcheckController);
    appLoggerService = module.get<AppLoggerService>(AppLoggerService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('healthcheck should be called and return status as pass ', async () => {
    jest.spyOn(appLoggerService, 'error').mockImplementation(jest.fn());
    var result = await controller.get();
    expect(result.status).toBe('pass');
  });
});
