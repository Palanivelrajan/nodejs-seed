import { Controller, Get } from '@nestjs/common';
import { AppLoggerService } from 'synapse.bff.core';
import { config } from './../../config';
@Controller()
export class HealthcheckController {
  private start: number;
  constructor(private readonly logger: AppLoggerService) {
    this.start = Date.now();
  }

  @Get('healthcheck')
  async get() {
    const now = Date.now();

    // function delay(ms: number) {
    //   return new Promise(resolve => setTimeout(resolve, ms));
    // };
    // await delay(10000);

    let message = {
      Title: 'THis is Health Check',
      Type: 'Error',
      Detail: 'This is detail of the log',
      Status: 'Status',
      Extension: '',
    };
    //throw new Error('Boom');
    //throw new BadRequestException("The Parameter 1 is not valid");
    //throw new HttpException('This is Http Exception', HttpStatus.FORBIDDEN);

    this.logger.error(message, '');

    return {
      status: 'pass',
      version: config.version,
      details: {
        uptime: `${Number((now - this.start) / 1000).toFixed(0)}`,
      },
    };
  }
}
