import { Module } from '@nestjs/common';
import { HealthcheckController } from './healthcheck.controller';
//import { LoggersModule } from 'synapse.bff.core';
//import { LoggerModule } from '../logger/app.logger.module'
@Module({
  imports: [],
  controllers: [HealthcheckController],
  providers: [],
})
export class HealthcheckModule {}
