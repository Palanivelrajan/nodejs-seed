import { AppDispatcher } from './app';
import { AppLoggerService } from 'synapse.bff.core';
const logger = new AppLoggerService('Index');

let message = {
  Title: 'Application Start',
  Type: 'Info',
  Detail: 'Application Start',
  Status: 'Status',
};

logger.log(message);

const dispatcher = new AppDispatcher();
dispatcher
  .dispatch()
  .then(() =>
    logger.log({
      Title: 'Application Up',
      Type: 'Info',
      Detail: 'pplication Up',
      Status: 'Status',
    }),
  )
  .catch(e => {
    logger.error(e.message, e.stack);
    process.exit(1);
  });
